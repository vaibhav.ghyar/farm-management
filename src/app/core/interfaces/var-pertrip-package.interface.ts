export interface IPERTRIP_PACKAGE {
    typeOfVehicle: string;
	baseKms: string;
	noOfHours: string;
	deliveryOwner: string;
	daRate: string
	extraKmsRate: string
	extraHoursRate: string
	billingDate: string;
	paymentDate: string;
	specialTerms: string;
	clientId : string;
	vendorId : number | null;
	deliveryCenterId : string;
}


