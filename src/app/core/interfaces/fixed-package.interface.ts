export interface IFIXED_PACKAGE {
    typeOfVehicle: string;
	baseKms: string;
	noOfDays: string;
	deliveryOwner: string;
	packageRate: string
	daRate: string
	extraKmsRate: string
	extraHoursRate: string
	onDemandTypeOfVehicle: string
	onDemandKmsRangeLower: string
	onDemandKmsRangeHigher: string
	onDemandRate: string
	billingDate: string;
	paymentDate: string;
	specialTerms: string;
	isOnDemand: string;
	clientId : string;
	vendorId : number | null;
	deliveryCenterId : string;
}


