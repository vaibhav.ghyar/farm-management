import { IFIXED_PACKAGE } from "./fixed-package.interface";
import { IPERCASE_PACKAGE } from "./var-percase-package.interface";
import { IPERORDER_PACKAGE } from "./var-perorder-package.interface";
import { IPERTRIP_PACKAGE } from "./var-pertrip-package.interface"
import { IPERKG_PACKAGE} from "./var-perkg-package.interface"
export { IFIXED_PACKAGE, IPERTRIP_PACKAGE, IPERCASE_PACKAGE, IPERORDER_PACKAGE,  IPERKG_PACKAGE}