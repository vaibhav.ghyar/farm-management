export interface IPERKG_PACKAGE {
	minimumGuaranteeKGPerMonth: number | undefined,
	tonnageRangeLower: number | undefined,
	tonnageRangeHigher: number | undefined,
	ratePerKG: number | undefined,
	billingDate: string,
	paymentDate: string,
	specialTerms:string,
}


