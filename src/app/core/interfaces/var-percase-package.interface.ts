export interface IPERCASE_PACKAGE {
	minimumGuaranteeCasesPerMonth: number | undefined,
	caseRangeLower: number | undefined,
	caseRangeHigher: number | undefined,
	ratePerCase: number | undefined,
	billingDate: string,
	paymentDate: string,
	specialTerms:string,
}


