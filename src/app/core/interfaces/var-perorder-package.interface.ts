export interface IPERORDER_PACKAGE {
	minimumGuaranteeOrderPerMonth: number | undefined,
	orderRangeLower: number | undefined,
	orderRangeHigher: number | undefined,
	ratePerOrder: number | undefined,
	billingDate: string,
	paymentDate: string,
	specialTerms:string,
}