import { Router } from '@angular/router';
// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../modules/auth/services/auth.service';

@Injectable()
export class InterceptService implements HttpInterceptor {

	constructor(private router: Router, private authServcie: AuthService) { }
	// intercept request and add token
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// modify request
		// let token = localStorage.getItem(environment.authTokenKey);
		const user = this.authServcie.currentUserValue;

		if (user) {
			const token = user.sessionToken;
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ${token}`
				}
			});
		} else {
			request = request.clone();
		}
		return next.handle(request).pipe(
			tap(
				event => {
					if (event instanceof HttpResponse) {
						console.log('all looks good');
						//when unautherize user then redirec to login page 
						if(event.body.statusCode===403){
							//this.openSnackBar(event.body.message,'')
							localStorage.clear();
							this.router.navigate(['auth/login']);					
						}
						

					}
				},
				error => {
					// http response status code
					// console.log('----response----');
					// console.error('status code:');
					// tslint:disable-next-line:no-debugger
					console.error(error.status);
					console.error(error.message);
					if (error.status === 422) {
						this.openSnackBar(error.error.error, '');
					}

					// navigate to login and clear localstorage if the token is expired or
					// unauthorized error is occured from the backend
					if (error.status === 403) {
						localStorage.clear();
						// this.store.dispatch(new Logout());
						this.router.navigate(['auth/login']);
					}
					// console.log('--- end of response---');
				}
			)
		);
	}
	openSnackBar(message: string, action: string) {
		// this.snackBar.open(message, action, {
		// 	duration: 2000,
		// });
	}
}
