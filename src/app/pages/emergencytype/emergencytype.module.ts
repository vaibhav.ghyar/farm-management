import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EmergencytypeComponent } from './emergencytype.component';

@NgModule({
  declarations: [EmergencytypeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: EmergencytypeComponent,
      },
    ]),
  ],
})
export class EmergencytypeModule {}