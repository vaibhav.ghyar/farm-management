import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencytypeComponent } from './emergencytype.component';

describe('EmergencytypeComponent', () => {
  let component: EmergencytypeComponent;
  let fixture: ComponentFixture<EmergencytypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencytypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencytypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
