import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms'; 
import { ApiService } from 'src/app/pages/farmerlist/api.service';
import { FarmerlistModel } from 'src/app/pages/farmerlist/farmerlist.Model';

@Component({
  selector: 'ooraja-farmerlist',
  providers: [ApiService  ],
  templateUrl: './farmerlist.component.html',
  styleUrls: ['./farmerlist.component.scss']
})
export class FarmerlistComponent implements OnInit {
  farmerlistModelObj:FarmerlistModel=new FarmerlistModel();
  
  
  formValue !: FormGroup;
  farmerlistData!:any;
  showAdd!:boolean;
  showUpdate!:boolean;
  
  
  showUpdateTitle!:boolean;
  showAddTitle!:boolean;
    constructor(private formBuilder:FormBuilder,private api:ApiService) { 
  
    }
  
    addButtonClickFunction(){
      this.formValue.reset();
      this.showUpdate=false;
      this.showAdd=true;
      this.showUpdateTitle=false;
      this.showAddTitle=true;
    }
  
  
  
    ngOnInit(): void {
  
    this.formValue=this.formBuilder.group({
  
    firstName:[''],
    lastName:[''],
    city:[''],
    Mobile:[''],
    AltMobile:[''],
  
        
      })  
  this.getAllFarmerlist();
  }
  
  postFarmerlistDetails(){
  //alert("fucntion call");
  this.farmerlistModelObj.id=this.formValue.value.id ;
  
    this.farmerlistModelObj.firstName=this.formValue.value.firstName;
    this.farmerlistModelObj.lastName=this.formValue.value.lastName;
    this.farmerlistModelObj.city=this.formValue.value.city;
    this.farmerlistModelObj.Mobile=this.formValue.value.Mobile;
    this.farmerlistModelObj.AltMobile=this.formValue.value.Mobile;
    
  
    let cancel=document.getElementById("cancel");
    this.api.postData(this.farmerlistModelObj).subscribe(a=> {
  
      console.log(a);
      alert("Record inserted successfully");
      cancel?.click();this.formValue.reset();
      this.getAllFarmerlist();
    })
   }
  
  
   getAllFarmerlist(){
    this.api.getData().subscribe(a=>{
      this.farmerlistData=a;
    })
  
  }
  deleteFarmerlist(row:any){
  
    this.api.deleteData(row.id).subscribe(a=>{
      alert("Record Deleted Succesfully");
      this.getAllFarmerlist();
    })
  
  }
  updateFarmerlist(row:any){
    this.showUpdate=true;
    this.showAdd=false;
  
    this.showUpdateTitle=true;
    this.showAddTitle=false;
    this.farmerlistModelObj.id=row.id;
    this.formValue.controls['firstName'].setValue(row.firstName);
    this.formValue.controls['lastName'].setValue(row.lastName);
    this.formValue.controls['city'].setValue(row.city);
    this.formValue.controls['Mobile'].setValue(row.Mobile);
    this.formValue.controls['AltMobile'].setValue(row.AltMobile);
 
  
  }
  
  updateFarmerlistDetails(){
  
    this.farmerlistModelObj.firstName=this.formValue.value.firstName;
    this.farmerlistModelObj.lastName=this.formValue.value.lastName;
    this.farmerlistModelObj.city=this.formValue.value.city;
    this.farmerlistModelObj.Mobile=this.formValue.value.Mobile;
    this.farmerlistModelObj.AltMobile=this.formValue.value.AltMobile;

    this.api.updateData(this.farmerlistModelObj,this.farmerlistModelObj.id).subscribe(a=>{
      alert("Record updated Succesfully");
  
    let cancel=document.getElementById("cancel");
  
      cancel?.click();
      this.formValue.reset();
      this.getAllFarmerlist();
    })
  }
  
  }
  
