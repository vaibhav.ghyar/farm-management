import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FarmerlistComponent } from 'src/app/pages/farmerlist/farmerlist.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FarmerlistComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: FarmerlistComponent,
      },
    ]),
  ],
})
export class FarmerlistModule {}