import { Routes } from '@angular/router';


const Routing: Routes = [
  { path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
  },

  {
    path: 'farmerlist',
    loadChildren: () => import('./farmerlist/farmerlist.module').then ((m) => m.FarmerlistModule),
  },

  {
    path: 'emergencytype',
    loadChildren: () => import('./emergencytype/emergencytype.module').then((m) => m.EmergencytypeModule),
  },

  {
    path: '',
    redirectTo: '/dashboard', pathMatch: 'full',
  },

  {
    path: '**', redirectTo: 'error/404',
  },
];

export { Routing };
